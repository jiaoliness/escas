<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function()
{

  Route::get('/home', 'HomeController@index')->name('home');

  Route::resource('customer', 'CustomerController');
  Route::resource('order', 'OrderController');
  Route::resource('package', 'PackageController');
  Route::resource('product', 'ProductController');

  Route::get('order/{order}/add/products', 'OrderController@addProductsView')->name('order-products');

  Route::post('order/{order}/add/products', 'OrderController@addProducts')->name('order-products');

});
