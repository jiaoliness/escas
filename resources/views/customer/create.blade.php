@extends('layouts.backend')

@section('content')

{{ generateBreadcrumbs($customer, 'customer') }}

<div class="col-md-12">
  <div class="card">
      <div class="card-header">
        @if($customer->id > 0)
          Edit Customer
        @else
          Create Customer
        @endif
      </div>
      <div class="card-body">

        @if($customer->id > 0)
            {{ Form::model($customer, ['route' => ['customer.update', $customer->id],
            'method' => 'put', 'class' => 'needs-validation']) }}

            @method('PUT')
        @else
            {{ Form::open(['route' => 'customer.store', 'class' => 'needs-validation']) }}
        @endif

        <div class="form-row">
          <div class="col-md-6">
            <div class="form-group">
              {!! Form::label('contact_name', 'Name') !!}
              {!! Form::text('contact_name', $customer->contact_name, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('company_name', 'Company') !!}
              {!! Form::text('company_name', $customer->company_name, ['class' => 'form-control']) !!}
            </div>
          </div>
          <div class="col-md-6">

            <div class="form-group">
              {!! Form::label('address', 'Address') !!}
              {!! Form::text('address', $customer->address, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('contact', 'Contact No.') !!}
              {!! Form::text('contact', $customer->contact, ['class' => 'form-control']) !!}
            </div>

          </div>
          <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('notes', 'Notes') !!}
                {!! Form::text('notes', $customer->notes, ['class' => 'form-control']) !!}
              </div>
          </div>
        </div>

        <button class="btn btn-primary" type="submit">Save</button>

        {{ Form::close() }}

      </div>
  </div>
</div>

@endsection
