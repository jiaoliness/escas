@extends('layouts.backend')

@section('content')

<div class="col-md-12">
  <div class="card">
    <div class="card-header card-header-primary">
      <h4 class="card-title">Customers Table</h4>
      <p class="card-category"><a href="{{ route('customer.create') }}">Add new</a></p>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table text-center dataTable table-striped">
          <thead class=" text-primary">
            <th class="text-left">Contact Name</th>
            <th>Company Name</th>
            <th>Address</th>
            <th>Contact No.</th>
            <th>View</th>
          </thead>
          <tbody>
            @foreach($customers as $customer)
              <tr>
                <td class="text-left">{{ $customer->contact_name }}</td>
                <td>{{ $customer->company_name }}</td>
                <td>{{ $customer->address }}</td>
                <td>{{ $customer->contact }}</td>
                <td>
                  <a href="{{ route('customer.show', ['customer' => $customer->id]) }}">
                    <i class="material-icons">account_circle</i>
                  </a>

                  <a href="">
                    <i class="material-icons">assignment</i>
                  </a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
