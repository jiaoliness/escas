@extends('layouts.backend')

@section('content')

{{ generateBreadcrumbs($order, 'order') }}

<div class="col-md-12">
  <div class="card">
    <div class="card-header">
      @if($order->id > 0)
        Edit order<br/>
        <a href="{{ route('order-products', ['order' => $order]) }}">Add Products to order</a>
      @else
        Create order
      @endif
    </div>
    <div class="card-body">

      @if($order->id > 0)
          {{ Form::model($order, ['route' => ['order.update', $order->id],
          'method' => 'put', 'class' => 'needs-validation']) }}
          @method('PUT')
      @else
          {{ Form::open(['route' => 'order.store', 'class' => 'needs-validation']) }}
      @endif

      <div class="form-row">
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('customer_id', 'Customer', ['class' => 'mb-0']) !!}

            {!! Form::select('customer_id', customers(), $order->customer_id,
            ['class' => 'custom-select', 'required' => 'required']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('guests', 'Guests') !!}
            {!! Form::text('guests', $order->guests, ['class' => 'form-control', 'required' => 'required']) !!}
          </div>

        </div>
        <div class="col-md-4">

          <div class="form-group">
            {!! Form::label('event_date', 'Event Date') !!}
            {!! Form::date('event_date', $order->event_date, ['class' => 'form-control', 'required' => 'required']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('time_start', 'Time Start') !!}
            {!! Form::time('time_start', $order->time_start, ['class' => 'form-control', 'required' => 'required']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('time_end', 'Time End') !!}
            {!! Form::time('time_end', $order->time_end, ['class' => 'form-control', 'required' => 'required']) !!}
          </div>

        </div>
        <div class="col-md-4">

          <div class="form-group">
            {!! Form::label('venue', 'Venue') !!}
            {!! Form::text('venue', $order->venue, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('motif', 'Motif') !!}
            {!! Form::text('motif', $order->motif, ['class' => 'form-control']) !!}
          </div>

        </div>

      </div>

      <button class="btn btn-primary" type="submit">Save</button>

      {{ Form::close() }}

    </div>
    </div>
</div>

@endsection
