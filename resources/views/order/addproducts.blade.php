@extends('layouts.backend')

@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('order.index') }}">Order</a></li>
    <li class="breadcrumb-item"><a href="{{ route('order.create') }}">Create Order</a></li>
    <li class="breadcrumb-item"><a href="{{ route('order.show', ['order' => $order]) }}">Edit Order</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add Products to Order</li>
  </ol>
</nav>

<div class="col-md-12">
  <div class="card">
    <div class="card-header">
      @if($order->id > 0)
        Edit order<br/>
        <a href="{{ route('order-products', ['order' => $order]) }}">Add Products to order</a>
      @else
        Create order
      @endif
    </div>
    <div class="card-body">

      @if($order->id > 0)
          {{ Form::model($order, ['route' => ['order.update', $order->id],
          'method' => 'put', 'class' => 'needs-validation']) }}
          @method('PUT')
      @else
          {{ Form::open(['route' => 'order.store', 'class' => 'needs-validation']) }}
      @endif


      <button class="btn btn-primary" type="submit">Save</button>

      {{ Form::close() }}

    </div>
    </div>
</div>

@endsection
