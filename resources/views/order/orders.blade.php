@extends('layouts.backend')

@section('content')

<div class="col-md-12">
  <div class="card">
    <div class="card-header card-header-primary">
      <h4 class="card-title">Orders Table</h4>
      <p class="card-category"><a href="{{ route('order.create') }}">Add new</a></p>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table text-center dataTable table-striped">
          <thead class=" text-primary">
            <th class="text-left">Customer Name</th>
            <th>Guests</th>
            <th>Event Date and Time</th>
            <th>Venue</th>
            <th>Motif</th>
            <th>Actions</th>
          </thead>
          <tbody>
            @foreach($orders as $order)
              <tr>
                <td class="text-left">
                  @if($order->company_name)
                    {{ $order->customer->contact_name }} -
                  @endif
                  {{ $order->customer->contact_name }}</td>
                <td>{{ $order->guests }}</td>
                <td>{{ readableDate($order->event_date) }}<br/>{{ rtime($order->time_start) }} - {{ rtime($order->time_end) }}</td>
                <td>{{ $order->venue }}</td>
                <td>{{ $order->motif }}</td>


                <td>
                  <a href="{{ route('order.show', ['order' => $order->id]) }}">
                    <i class="material-icons">account_circle</i>
                  </a>

                  <a href="">
                    <i class="material-icons">assignment</i>
                  </a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
