@extends('layouts.backend')

@section('content')

<div class="col-md-12">
  <div class="card">
    <div class="card-header card-header-primary">
      <h4 class="card-title">Products Table</h4>
      <p class="card-category"><a href="{{ route('product.create') }}">Add new</a></p>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table text-center dataTable table-striped">
          <thead class="text-primary">
            <th class="text-left">Name</th>
            <th>Quantity</th>
            <th>Suggested Price</th>
            <th>Food Type</th>
            <th>Category</th>
            <th>View</th>
          </thead>
          <tbody>
            @foreach($products as $product)
              <tr>
                <td class="text-left">{{ $product->name }}</td>
                <td>{{ quantity($product->quantity) }}</td>
                <td>{{ $product->suggested_price }}</td>
                <td>{{ food_category($product->food_category) }}</td>
                <td>{{ specialty($product->special) }}</td>
                <td>
                  <a href="{{ route('product.show', ['product' => $product->id]) }}">
                    <i class="material-icons">account_circle</i>
                  </a>

                  <a href="">
                    <i class="material-icons">assignment</i>
                  </a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
