@extends('layouts.backend')

@section('content')

{{ generateBreadcrumbs($product, 'product') }}

<div class="col-md-12">
  <div class="card">
    <div class="card-header">
      @if($product->id > 0)
        Edit Product
      @else
        Create Product
      @endif
    </div>
    <div class="card-body">

      @if($product->id > 0)
          {{ Form::model($product, ['route' => ['product.update', $product->id],
          'method' => 'put', 'class' => 'needs-validation']) }}

          @method('PUT')
      @else
          {{ Form::open(['route' => 'product.store', 'class' => 'needs-validation']) }}
      @endif

      <div class="form-row">
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', $product->name, ['class' => 'form-control', 'required' => 'required']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('food_category', 'Food Type', ['class' => 'mb-0']) !!}
            {!! Form::select('food_category', food_category(), $product->food_category,
            ['class' => 'custom-select', 'required' => 'required']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('suggested_price', 'Suggested Price') !!}
            {!! Form::text('suggested_price', $product->suggested_price, ['class' => 'form-control']) !!}
          </div>

        </div>
        <div class="col-md-4">

          <div class="form-group">
            {!! Form::label('special', 'Category', ['class' => 'mb-0']) !!}
            {!! Form::select('special', specialty(), $product->special,
            ['class' => 'custom-select', 'required' => 'required']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('quantity', 'Quantity', ['class' => 'mb-0']) !!}
            {!! Form::select('quantity', quantity(), $product->quantity,
            ['class' => 'custom-select', 'required' => 'required']) !!}
          </div>

        </div>
        <div class="col-md-4">

        </div>

      </div>

      <button class="btn btn-primary" type="submit">Save</button>

      {{ Form::close() }}

    </div>
    </div>
</div>

@endsection
