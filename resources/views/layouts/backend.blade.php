<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Escas') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/material-dashboard.min.css') }}" rel="stylesheet">

    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    @yield('styles')
</head>
<body>

    <div class="wrapper">
      <div class="sidebar" data-color="purple" data-background-color="purple">
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

          Tip 2: you can also add an image using data-image tag
      -->
        <div class="logo">
          <a class="simple-text logo-normal">
            Escas
          </a>
        </div>
        <div class="sidebar-wrapper">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('home') }}">
                <i class="material-icons">calendar_today</i>
                <p>Events</p>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="{{ route('customer.index') }}">
                <i class="material-icons">person</i>
                <p>Customers</p>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="{{ route('product.index') }}">
                <i class="material-icons">content_paste</i>
                <p>Products</p>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="{{ route('package.index') }}">
                <i class="material-icons">content_paste</i>
                <p>Packages</p>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="{{ route('order.index') }}">
                <i class="material-icons">library_books</i>
                <p>Orders</p>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="main-panel">

        <div class="content">
          <div class="container-fluid">
            <div class="row">
              @yield('content')
            </div>
          </div>
        </div>
      </div>
</div>
</body>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" defer></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js" defer></script>
<script src="{{ asset('js/scripts.js') }}"></script>
@yield('scripts')
</html>
