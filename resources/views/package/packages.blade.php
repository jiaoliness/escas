@extends('layouts.backend')

@section('content')

<div class="col-md-12">
  <div class="card">
    <div class="card-header card-header-primary">
      <h4 class="card-title">Packages Table</h4>
      <p class="card-category"><a href="{{ route('package.create') }}">Add new</a></p>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table text-center dataTable table-striped">
          <thead class="text-primary">
            <th class="text-left">Name</th>
            <th>Quantity</th>
            <th>Suggested Price</th>
            <th>Food Type</th>
            <th>Category</th>
            <th>View</th>
          </thead>
          <tbody>
            @foreach($packages as $package)
              <tr>
                <td class="text-left">{{ $package->name }}</td>
                <td>{{ quantity($package->quantity) }}</td>
                <td>{{ $package->suggested_price }}</td>
                <td>{{ food_category($package->food_category) }}</td>
                <td>{{ specialty($package->special) }}</td>
                <td>
                  <a href="{{ route('package.show', ['package' => $package->id]) }}">
                    <i class="material-icons">account_circle</i>
                  </a>

                  <a href="">
                    <i class="material-icons">assignment</i>
                  </a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
