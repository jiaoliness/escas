@extends('layouts.backend')

@section('styles')
<link href='{{ asset("calendar/packages/core/main.css") }}'rel='stylesheet'/>
<link href='{{ asset("calendar/packages/daygrid/main.css") }}'rel='stylesheet'/>
<link href='{{ asset("calendar/packages/timegrid/main.css") }}'rel='stylesheet'/>
<link href='{{ asset("calendar/packages/list/main.css") }}'rel='stylesheet'/>
@endsection

@section('content')
<div class="col-md-12">
<div id='calendar'></div>
</div>
@endsection

@section('scripts')
<script src='{{ asset("calendar/packages/core/main.js") }}'></script>
<script src='{{ asset("calendar/packages/interaction/main.js") }}'></script>
<script src='{{ asset("calendar/packages/daygrid/main.js") }}'></script>
<script src='{{ asset("calendar/packages/timegrid/main.js") }}'></script>
<script src='{{ asset("calendar/packages/list/main.js") }}'></script>

<script>
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
      },

      defaultDate: '2019-03-12',
      navLinks: true, // can click day/week names to navigate views
      businessHours: true, // display business hours
      editable: false,
      events: [
        {
          title: 'Business Lunch',
          start: '2019-03-03T13:00:00',
          constraint: 'businessHours'
        },
        {
          title: 'Meeting',
          start: '2019-03-13T11:00:00',
          constraint: 'availableForMeeting', // defined below
          color: '#257e4a'
        },
        {
          title: 'Conference',
          start: '2019-03-18',
          end: '2019-03-20'
        },
        {
          title: 'Party',
          start: '2019-03-29T20:00:00'
        },

        // areas where "Meeting" must be dropped
        {
          groupId: 'availableForMeeting',
          start: '2019-03-11T10:00:00',
          end: '2019-03-11T16:00:00',
          rendering: 'background'
        },
        {
          groupId: 'availableForMeeting',
          start: '2019-03-13T10:00:00',
          end: '2019-03-13T16:00:00',
          rendering: 'background'
        },

        // red areas where no events can be dropped
        {
          start: '2019-03-24',
          end: '2019-03-28',
          overlap: false,
          rendering: 'background',
          color: '#ff9f89'
        },
        {
          start: '2019-03-06',
          end: '2019-03-08',
          overlap: false,
          rendering: 'background',
          color: '#ff9f89'
        }
      ]
    });

    calendar.render();
  });
</script>

<style>

  body {
    margin: 40px 10px;
    padding: 0;
    font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
    font-size: 14px;
  }

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

</style>
@endsection
