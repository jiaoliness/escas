<?php

use Illuminate\Support\Facades\DB;

use App\Customer;
use App\Order;
use App\Product;
use App\Package;

use Carbon\Carbon;

function food_category($id = -1)
{
  $cat[] = 'Chicken'; //1
  $cat[] = 'Pork'; //2
  $cat[] = 'Beef'; //3
  $cat[] = 'Fish and Seafood'; //4
  $cat[] = 'Noodles, Pasta, and Paella'; //5
  $cat[] = 'Vegetables'; //6
  $cat[] = 'Soup'; //7
  $cat[] = 'Salad and Dessert'; //8
  $cat[] = 'Appetizers'; //9

  return $id>-1 ? $cat[$id] : $cat;
}

function specialty($id = -1)
{
  $s[] = 'Buffet - Regular';
  $s[] = 'Buffet - Special';
  $s[] = 'Kids party';
  $s[] = 'Packed meal';
  $s[] = 'For pickup/order';
  return $id>-1 ? $s[$id] : $s;
}

function readableDate($date)
{
  return DateTime::createFromFormat('Y-m-d', $date)->format('M j, Y');
}

function rtime($time)
{
  return DateTime::createFromFormat('H:i:s', $time)->format('g:i A');
}

function quantity($id = -1)
{
  $pax[] = 'N/A';
  $pax[] = '25-30';
  $pax[] = '50-60';
  return $id>-1 ? $pax[$id] : $pax;
}

function generateBreadcrumbs($model, $name)
{
?>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">

      <li class="breadcrumb-item">
        <a href="<?php echo route($name.'.index') ?>"><?php echo ucwords($name) ?></a>
      </li>

    <?php if($model->id > 0){ ?>

      <li class="breadcrumb-item">
        <a href="<?php echo route($name.'.create') ?>">Create <?php echo ucwords($name) ?></a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Edit <?php echo ucwords($name) ?></li>

    <?php }else{ ?>

        <li class="breadcrumb-item active" aria-current="page">Create <?php echo ucwords($name) ?></li>

    <?php } ?>

    </ol>
  </nav>

<?php
}

function customers()
{
  $array = [];

  foreach(Customer::all() as $customer)
  {
    $array[$customer->id] = $customer->contact_name;
  }

  return $array;
}
