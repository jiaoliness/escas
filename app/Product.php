<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $guarded = ['id'];

  public function order()
  {
    return $this->belongsToMany('App\Order')->withPivot('id', 'price_given');
  }

}
