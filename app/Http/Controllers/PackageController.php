<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index()
      {
          $packages = Package::all();
          return view('package.packages', compact('packages'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function create()
      {
          $package = new Package;
          return view('package.create', compact('package'));
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          $package = new Package;
          $package->fill($request->all());
          $package->save();

          return redirect()->route('package.show', ['package' => $package]);
      }

      /**
       * Display the specified resource.
       *
       * @param  \App\Package  $package
       * @return \Illuminate\Http\Response
       */
      public function show(Package $package)
      {
          return view('package.create', compact('package'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  \App\Package  $package
       * @return \Illuminate\Http\Response
       */
      public function edit(Package $package)
      {
          //
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  \App\Package  $package
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, Package $package)
      {
          $package->update($request->all());
          return back();
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  \App\Package  $package
       * @return \Illuminate\Http\Response
       */
      public function destroy(Package $package)
      {
          $package->delete();
          return back();
      }
}
