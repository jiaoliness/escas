<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
  protected $guarded = ['id'];

  public function orders()
  {
    return $this->belongsToMany('App\Order')->withPivot('id','price', 'guests');
  }
}
