<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $guarded = ['id'];

  public function customer()
  {
    return $this->belongsTo('App\Customer');
  }

  public function products()
  {
    return $this->belongsToMany('App\Product')->withPivot('id', 'price_given');
  }

  public function packages()
  {
    return $this->belongsToMany('App\Package')->withPivot('id','price', 'guests');
  }
}
